package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	static Object[][] data;
	
	//public static void main(String[] args) throws IOException { 
	// remove main and 
	public static Object[][] readData()
	{
		// Enter Workbook
		XSSFWorkbook wbook;

		try {
			wbook = new XSSFWorkbook("./Data/TC001.xlsx");
			XSSFSheet sheet = wbook.getSheet("Sheet1");
			
			int rowcount = sheet.getLastRowNum();
			System.out.println("The Row count exluding the header is " +rowcount);
			
			int colcount = sheet.getRow(0).getLastCellNum(); 
			// This returns the number of columns in the header row
			System.out.println("The Column count is " +colcount);
			
			data = new Object[rowcount][colcount];
			// Variable data created to store the data in a 2 dim array
			
			
			for (int i = 1; i <= rowcount; i++) { 
				//iterating from i=1 since header row is index 0 and that is ignored
				XSSFRow row = sheet.getRow(i);
				
				for (int j = 0; j < colcount; j++) {
					// iterating every cell in the row. colcount would return the number of columns
					XSSFCell cell = row.getCell(j);
					//System.out.println(stringCellValue);
					data[i-1][j] = cell.getStringCellValue();
					// to store the value in the array
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

}
