package utils;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

	public class BasicReport {
		
		ExtentHtmlReporter html;
		ExtentReports extent;
		ExtentTest test;
		@Test
		public void runReport()
		{
			html = new ExtentHtmlReporter("./reports/report.html");
			extent = new ExtentReports();
			extent.attachReporter(html); //attaching the report created by the ExtentHtmlReporter in the given path
			test = extent.createTest("TC01_login", "Login to Leaftaps"); 
			// Create Test is the overloaded method where different parameters can be passed.
			// Here we have passed the test case name and description
			test.assignAuthor("SPC");
			test.assignCategory("SmokeTest");
			try {
				test.fail("TestCase Failure", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
				//Screenshot is created by the MdeiaEntityBuilder and stored in the path
			} catch (IOException e) {
				e.printStackTrace();
			} // Always surround the failure cases within the try catch block, since it throws an IO exception
		
			
			test.pass("This Case has passed");
			test.warning("This is the warning scenario");
			extent.flush(); // This is the one which moves the report to the outfile
		}
		
	}
