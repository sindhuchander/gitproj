package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bi.framework.design.ProjectMethods;

public class TC03_FindLead extends ProjectMethods {

@Test(groups="Regression")
public void FindLead() {
	
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUsername = locateElement("id", "username");
	clearAndType(eleUsername, "DemoSalesManager"); 
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, "crmsfa"); 
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin); 
		
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
	   	 click(eleCRM);
		WebElement eleLeads = locateElement("linktext", "Leads");
		click(eleLeads);
		
		/*
		WebElement eleFindLead = locateElement("xpath", "(//*[@text() ='Find Leads'])[1]");
    	 click(eleFindLead);*/
    	
		locateElement("linktext", "Find Leads").click(); 
				//driver.findElementByLinkText("Find Leads").click();
		
		clearAndType((locateElement("xpath", "(//input[@name='firstName'])[3]")),"Titan");
				//driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Titan");
		
   	 	locateElement("xpath", "//button[text()=\"Find Leads\"]").click();
   	 			// driver.findElementByXPath("//button[text()=\"Find Leads\"]").click();
   	 	
   	 WebElement eleFindButton = locateElement("xpath", "(//[@text() = 'Find Leads'])[2]");
   	 click(eleFindButton);	
	
}
}