
package bl.framework.testcases;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import bi.framework.design.ProjectMethods;

public class LoginForDataProvider extends ProjectMethods{
	
		@Test(dataProvider = "fetchData")
		public void CreateLead(String cname, String fname, String lname) {
				
				//click CRM/SFA link
				WebElement eleLink = locateElement("linktext", "CRM/SFA");
				click(eleLink);
				
				WebElement eleCreatelink = locateElement("linktext", "Create Lead");
				click(eleCreatelink);
				
				//Company Name 
				WebElement eleText = locateElement("id", "createLeadForm_companyName");
				clearAndType(eleText, cname);
				// cname is the data provider parameter moved from fetchdata() below
				
				//First Name : driver.findElementById("createLeadForm_firstName").sendKeys("Titan");
				clearAndType(locateElement("id", "createLeadForm_firstName"), fname);
				// fname is the data provider parameter moved from fetchdata() below
				
				// LastName :driver.findElementById("createLeadForm_lastName").sendKeys("Raga");
				clearAndType(locateElement("id", "createLeadForm_lastName"), lname); 
				// lname is the data provider parameter moved from fetchdata() below
				
				// Country Selection		
				selectDropDownUsingText(locateElement("id", "createLeadForm_generalCountryGeoId"), "India");
				
				// Source Dropdown Selection - SelectBY Index
				selectDropDownUsingIndex(locateElement("id", "createLeadForm_dataSourceId"), 2);
				
				// Marketing campaign - selectBYValue
				selectDropDownUsingValue(locateElement("id", "createLeadForm_marketingCampaignId"), "CATRQ_CARNDRIVER");
				
				// Submit button
				driver.findElementByName("submitButton").click();
				
				driver.close();
			}
		
		/*@DataProvider(name = "getData")
		public String[][] fetchdata()
		{
			String[][] data = new String[2][3]; // 2 rows and 3 cols
			data[0][0] = "TestLeaf";
			data[0][1] = "Titan";
			data[0][2] = "Raga1";
			
			data[1][0] = "TestLeaf";
			data[1][1] = "Titan";
			data[1][2] = "Raga2";
			
			return data;
		}*/
		
}


