package bl.framework.testcases;

import org.testng.annotations.Test;
import bi.framework.design.ProjectMethods;
import org.openqa.selenium.WebElement;
public class TC002_Editlead extends ProjectMethods {

//	@Test
/*	public void Login()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
	}
*/	//@Test(dependsOnMethods = "bl.framework.testcases.TC001_LoginAndLogout.CreateLead")
	//@Test(dependsOnMethods = "Login")
	@Test (groups = "Sanity")
	public void EditLead() {
		login();
		//login(); Since login method is called with Before Method annotation - it will be called under project methods itself
		//hence explicit calling of login is not required here
	
		
	//	WebElement eleLogout = locateElement("class", "decorativeSubmit");
	//  click(eleLogout);
		
		//click CRM/SFA link
		WebElement eleLink = locateElement("linktext", "CRM/SFA");
		click(eleLink);
		
		WebElement eleCreatelink = locateElement("linktext", "Create Lead");
		click(eleCreatelink);
		
		//Company Name 
		WebElement eleText = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleText, "Testleaf");
		//append(eleText, "BNYM");
		
		//First Name : driver.findElementById("createLeadForm_firstName").sendKeys("Titan");
		clearAndType(locateElement("id", "createLeadForm_firstName"), "Titan");

		// LastName :driver.findElementById("createLeadForm_lastName").sendKeys("Raga");
		clearAndType(locateElement("id", "createLeadForm_lastName"), "Raga");
		
		// Country Selection		
		selectDropDownUsingText(locateElement("id", "createLeadForm_generalCountryGeoId"), "India");
		
		// Source Dropdown Selection - SelectBY Index
		selectDropDownUsingIndex(locateElement("id", "createLeadForm_dataSourceId"), 2);
		
		// Marketing campaign - selectBYValue
		selectDropDownUsingValue(locateElement("id", "createLeadForm_marketingCampaignId"), "CATRQ_CARNDRIVER");
		
		// Submit button
		driver.findElementByName("submitButton").click();
	
		locateElement("linktext", "Edit").click(); //driver.findElementByLinkText("Edit").click();
		clearAndType(locateElement("id", "updateLeadForm_firstNameLocal"), "Testing"); //driver.findElementById("updateLeadForm_firstNameLocal").sendKeys("Testing");
		verifySelected(locateElement("id", "updateLeadForm_industryEnumId")); //isSelected();
		verifyDisplayed(locateElement("id", "updateLeadForm_birthDate")); //isDisplayed
		//verifyEnabled();
		locateElement("name", "submitButton").click(); //driver.findElementByName("submitButton").click();
		
	}

}
