package bl.framework.testcases;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;
import bi.framework.design.ProjectMethods;

public class LoginforParameter extends ProjectMethods {

	@Parameters({"URL", "Username", "Password"})
	@Test()
	public void CreateLead(String url, String uname, String pwd) {
			//login(); Since login method is called with Before Method annotation - it will be called under project methods itself
					//hence explicit calling of login is not required here
			startApp("chrome", url);
			WebElement eleUsername = locateElement("id", "username");
			clearAndType(eleUsername, uname); 
			WebElement elePassword = locateElement("id", "password");
			clearAndType(elePassword, pwd); 
			WebElement eleLogin = locateElement("class", "decorativeSubmit");
			click(eleLogin); 
			
		//	WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//  click(eleLogout);
			
			//click CRM/SFA link
			WebElement eleLink = locateElement("linktext", "CRM/SFA");
			click(eleLink);
			
			WebElement eleCreatelink = locateElement("linktext", "Create Lead");
			click(eleCreatelink);
			
			//Company Name 
			WebElement eleText = locateElement("id", "createLeadForm_companyName");
			clearAndType(eleText, "Testleaf");
			//append(eleText, "BNYM");
			
			//First Name : driver.findElementById("createLeadForm_firstName").sendKeys("Titan");
			clearAndType(locateElement("id", "createLeadForm_firstName"), "Titan");

			// LastName :driver.findElementById("createLeadForm_lastName").sendKeys("Raga");
			clearAndType(locateElement("id", "createLeadForm_lastName"), "Raga");
			
			// Country Selection		
			selectDropDownUsingText(locateElement("id", "createLeadForm_generalCountryGeoId"), "India");
			
			// Source Dropdown Selection - SelectBY Index
			selectDropDownUsingIndex(locateElement("id", "createLeadForm_dataSourceId"), 2);
			
			// Marketing campaign - selectBYValue
			selectDropDownUsingValue(locateElement("id", "createLeadForm_marketingCampaignId"), "CATRQ_CARNDRIVER");
			
			// Submit button
			driver.findElementByName("submitButton").click();
		}
	
	
	}

